# Faculty take-home consulting project- Jacob Cutts

<h2><a href="https://faculty-dashboard-jacob.netlify.app/">Netifly Deployment</a></h2>

<a href="https://github.com/WeAreAcademy/academy-react-starter">Built using the Academy-react-starter</a> <br> 
<a href="https://www.notion.so/Faculty-Dashboard-a6bf32491cb74ae7b17e5d8b1407045f">Full Documentation here on notion</a> <br>

<p>Hi, heres my take on the project. Had a lot of fun making it and the useReducer is my new favourite hook- helped massively with code organisation.
Thanks for the oppurtunity and I hope you like the app.</p>

<h2>Extras:</h2>
<ul>CI/CD, Jest and Cypress testing. Tests can be found on the notion page</ul>
<ul>User can see their recently viewed clients and employees. Built using local storage</ul>
<ul>References to employees/clients are all links for quick navigation</ul>
<ul>Filters tag cloud to easily track filters and remove them</ul>
<ul>Revenue graphs</ul>
<ul>Navigation using react router</ul>
<ul>Built with react-bootstrap</ul>
<ul>Id lookup pages</ul>

<h2>Known Bugs: </h2>
<ul>Filters not readable on mobile</ul>
<ul>Size filter input does not clear on clear filters click</ul>
