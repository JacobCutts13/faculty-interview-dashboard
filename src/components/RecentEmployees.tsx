import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { getEmployeeNames } from "../utils/getEmployeesName";
import { Employee } from "../interfaces";
interface Props {
  recentEmployees: string[];
  allEmployees: Employee[];
}

export default function RecentEmployees({
  recentEmployees,
  allEmployees,
}: Props): JSX.Element {
  const recentEmployeesWithNames = recentEmployees.map((id) => ({
    id: id,
    name: getEmployeeNames(allEmployees, [id])[0],
  }));
  return (
    <>
      <ListGroup variant="flush">
        {recentEmployeesWithNames.map((employee) => (
          <ListGroup.Item key={employee.id} className="d-flex">
            <h2>
              {employee.name}:
              <Link
                onClick={() => window.scrollTo(0, 0)}
                to={"/employees/" + employee.id}
              >
                {employee.id}
              </Link>
            </h2>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </>
  );
}
