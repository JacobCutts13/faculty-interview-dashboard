import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { getClientName } from "../utils/getClientName";
import { Client } from "../interfaces";
interface Props {
  recentClients: string[];
  allClients: Client[];
}

export default function RecentClients({
  recentClients,
  allClients,
}: Props): JSX.Element {
  const recentClientsWithNames: Client[] = recentClients.map((id) => ({
    id: id,
    name: getClientName(allClients, id),
  }));
  return (
    <>
      <ListGroup variant="flush">
        {recentClientsWithNames.map((client) => (
          <ListGroup.Item key={client.id} className="d-flex">
            <h2>
              {client.name}:
              <Link
                onClick={() => window.scrollTo(0, 0)}
                to={"/clients/" + client.id}
              >
                {client.id}
              </Link>
            </h2>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </>
  );
}
