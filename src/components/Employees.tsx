import { Dispatch } from "react";
import { State, Action } from "../interfaces";
import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function Employees({ state, dispatch }: Props): JSX.Element {
  return (
    <div>
      <div>
        <h2>Id Lookup</h2>
        <ListGroup variant="flush">
          {state.employees.map((employee) => (
            <ListGroup.Item key={employee.id} className="d-flex">
              <h2>
                {employee.name}:{" "}
                <Link
                  onClick={() => window.scrollTo(0, 0)}
                  to={"/employees/" + employee.id}
                >
                  {employee.id}
                </Link>
              </h2>
            </ListGroup.Item>
          ))}
        </ListGroup>
      </div>
    </div>
  );
}
