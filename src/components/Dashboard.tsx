import { Dispatch } from "react";
import { Action, State } from "../interfaces";
import Project from "./Project";
import { ListGroup } from "react-bootstrap";
import Revenue from "./Revenue";
import SearchProjects from "./SearchProjects";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function Dashboard({ state, dispatch }: Props): JSX.Element {
  return (
    <div>
      <Revenue
        allProjects={state.projectsWithNames}
        selectedProjects={state.filteredProjects}
      />
      <SearchProjects state={state} dispatch={dispatch} />
      {state.filteredProjects.length > 0 && (
        <ListGroup variant="flush" className="mt-5">
          <h1 className="mx-3 mt-5">Consultancy Projects: </h1>
          {state.filteredProjects.map((project) => (
            <ListGroup.Item key={project.id} id="dashboard-projects">
              <Project project={project} />
            </ListGroup.Item>
          ))}
        </ListGroup>
      )}
    </div>
  );
}
