import axios from "axios";
import { Dispatch, useEffect } from "react";
import { Link } from "react-router-dom";
import { ListGroup } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { State, Action } from "../interfaces";
import filterProjectsByClient from "../utils/filterProjectsbyClient";
import Project from "./Project";
import { CLIENTS_KEY } from "../objects";
import Revenue from "./Revenue";
import getUniqueEmployees from "../utils/getUniqueEmployees";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function Client({ state, dispatch }: Props): JSX.Element {
  const { clientid } = useParams();
  const clientsProjects = clientid
    ? filterProjectsByClient(clientid, state.projectsWithNames)
    : [];

  useEffect(() => {
    const getClientName = async () => {
      const nameRes = await axios.get(
        "https://consulting-projects.academy-faculty.repl.co/api/clients/" +
          clientid
      );
      dispatch({ type: "setCurrentClient", client: nameRes.data });
    };
    if (clientid) {
      getClientName();
      if (!state.recentClients.includes(clientid)) {
        localStorage.setItem(
          CLIENTS_KEY,
          JSON.stringify([...state.recentClients, clientid])
        );
        dispatch({
          type: "setRecentClients",
          recentClients: [...state.recentClients, clientid],
        });
      }
    }
  }, [clientid, dispatch, state.recentClients]);
  const uniqueEmployees = getUniqueEmployees(clientsProjects);
  return (
    <div>
      <h1>{state.currentClient.name}</h1>
      <Revenue
        allProjects={state.projectsWithNames}
        selectedProjects={clientsProjects}
      />
      <h3>Worked With: </h3>
      {uniqueEmployees.map((employee) => (
        <Link
          onClick={() => window.scrollTo(0, 0)}
          key={employee.id}
          to={"/employees/" + employee.id}
        >
          {employee.name + " | "}
        </Link>
      ))}
      <ListGroup variant="flush">
        <h1 className="mx-3 mt-5">Consultancy Projects: </h1>
        {clientsProjects.map((project) => (
          <ListGroup.Item key={project.id}>
            <Project project={project} />
          </ListGroup.Item>
        ))}
      </ListGroup>
    </div>
  );
}
