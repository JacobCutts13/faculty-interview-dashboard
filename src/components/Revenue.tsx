import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { ProjectWithNames } from "../interfaces";
import calcRevenue from "../utils/calcRevenue";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  indexAxis: "y" as const,
  elements: {
    bar: {
      borderWidth: 2,
    },
  },
  responsive: true,
  plugins: {
    legend: {
      position: "right" as const,
    },
    title: {
      display: true,
      text: "Aggregate Project Revenue",
    },
  },
};
const labels = ["Revenue"];

interface Props {
  allProjects: ProjectWithNames[];
  selectedProjects: ProjectWithNames[];
}

export default function Revenue({
  allProjects,
  selectedProjects,
}: Props): JSX.Element {
  const allRevenue = calcRevenue(allProjects);
  const selectedRevenue = calcRevenue(selectedProjects);
  const data = {
    labels,
    datasets: [
      {
        label: "Total: £" + allRevenue.toFixed(2),
        data: [allRevenue],
        borderColor: "rgb(255, 99, 132)",
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
      {
        label: "Selected: £" + selectedRevenue.toFixed(2),
        data: [selectedRevenue],
        borderColor: "rgb(53, 162, 235)",
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
    ],
  };
  return (
    <div className="mx-auto" style={{ width: "800px" }}>
      <Bar options={options} data={data} />
    </div>
  );
}
