import { Dispatch } from "react";
import { State, Action } from "../interfaces";
import { Container, Button } from "react-bootstrap";
import removeFilter from "../utils/removeFilter";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function FilterCloud({ state, dispatch }: Props): JSX.Element {
  return (
    <Container fluid className="d-flex m-5  px-5 justify-content">
      {state.sort !== "" && (
        <div className="mx-2 p-1">
          <Button
            variant="success"
            onClick={() => dispatch({ type: "setSort", sort: "" })}
          >
            {state.sort} &#8597;
          </Button>
        </div>
      )}
      {state.filters.clients.length > 0 && (
        <div className="mx-2 p-1 border">
          {state.filters.clients.map((client, index) => (
            <Button
              className="m-1"
              variant="success"
              onClick={() => removeFilter(state, dispatch, "client", client)}
              key={client + index}
            >
              {client}
            </Button>
          ))}
        </div>
      )}
      {state.filters.employees.length > 0 && (
        <div className="mx-2 p-1 border">
          {state.filters.employees.map((employee, index) => (
            <Button
              className="m-1"
              variant="success"
              onClick={() =>
                removeFilter(state, dispatch, "employee", employee)
              }
              key={employee + index}
            >
              {employee}
            </Button>
          ))}
        </div>
      )}

      {state.filters.minSize !== -1 && (
        <Button
          className="m-1"
          variant="success"
          onClick={() =>
            dispatch({
              type: "setFilters",
              filters: { ...state.filters, minSize: -1 },
            })
          }
        >
          {"Min: £" + state.filters.minSize}
        </Button>
      )}
      {state.filters.maxSize !== -1 && (
        <Button
          className="m-1"
          variant="success"
          onClick={() =>
            dispatch({
              type: "setFilters",
              filters: { ...state.filters, maxSize: -1 },
            })
          }
        >
          {"Max: £" + state.filters.maxSize}
        </Button>
      )}

      {(state.filters.startedFrom !== "any" ||
        state.filters.startedTo !== "any") && (
        <div className="mx-2 p-1 border">
          {state.filters.startedFrom !== "any" && (
            <Button
              className="m-1"
              variant="success"
              onClick={() =>
                dispatch({
                  type: "setFilters",
                  filters: { ...state.filters, startedFrom: "any" },
                })
              }
            >
              Started After:{" "}
              {state.filters.startedFrom
                .toString()
                .split(" ")
                .slice(0, 4)
                .join(" ")}
            </Button>
          )}
          {state.filters.startedTo !== "any" && (
            <Button
              className="m-1"
              variant="success"
              onClick={() =>
                dispatch({
                  type: "setFilters",
                  filters: { ...state.filters, startedTo: "any" },
                })
              }
            >
              Started Before:{" "}
              {state.filters.startedTo
                .toString()
                .split(" ")
                .slice(0, 4)
                .join(" ")}
            </Button>
          )}
        </div>
      )}
      {(state.filters.to !== "any" || state.filters.from !== "any") && (
        <div className="mx-2 p-1 border">
          {state.filters.from !== "any" && (
            <Button
              className="m-1"
              variant="success"
              onClick={() =>
                dispatch({
                  type: "setFilters",
                  filters: { ...state.filters, from: "any" },
                })
              }
            >
              Ended After:{" "}
              {state.filters.from.toString().split(" ").slice(0, 4).join(" ")}
            </Button>
          )}
          {state.filters.to !== "any" && (
            <Button
              className="m-1"
              variant="success"
              onClick={() =>
                dispatch({
                  type: "setFilters",
                  filters: { ...state.filters, to: "any" },
                })
              }
            >
              Ended Before:{" "}
              {state.filters.to.toString().split(" ").slice(0, 4).join(" ")}
            </Button>
          )}
        </div>
      )}
    </Container>
  );
}
