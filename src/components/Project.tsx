import { ProjectWithNames } from "../interfaces";
import { Link } from "react-router-dom";

interface Props {
  project: ProjectWithNames;
}

export default function Project({ project }: Props): JSX.Element {
  return (
    <div className="project-container">
      <h1 className="project-title">
        <Link
          onClick={() => window.scrollTo(0, 0)}
          to={"/clients/" + project.clientId}
        >
          {project.clientName}
        </Link>
        - £{project.contract.size}
      </h1>
      <h2>
        {project.contract.startDate} - {project.contract.endDate}
      </h2>
      <h3>Project Id: {project.id}</h3>
      <h4>
        Employees:{" "}
        {project.employeeIds.map((id, index) => (
          <Link
            onClick={() => window.scrollTo(0, 0)}
            key={id}
            to={"/employees/" + id}
          >
            {project.employeeNames[index] + " | "}
          </Link>
        ))}
      </h4>
    </div>
  );
}
