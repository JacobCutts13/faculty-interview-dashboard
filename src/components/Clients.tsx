import { Dispatch } from "react";
import { State, Action } from "../interfaces";
import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function Clients({ state, dispatch }: Props): JSX.Element {
  return (
    <div id="clients">
      <h2>Id Lookup</h2>
      <ListGroup variant="flush">
        {state.clients.map((client) => (
          <ListGroup.Item key={client.id} className="d-flex">
            <h2>
              {client.name}:{" "}
              <Link
                onClick={() => window.scrollTo(0, 0)}
                to={"/clients/" + client.id}
              >
                {client.id}
              </Link>
            </h2>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </div>
  );
}
