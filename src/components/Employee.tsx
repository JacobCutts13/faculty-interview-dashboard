import axios from "axios";
import { Dispatch, useEffect } from "react";
import { Image, ListGroup } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { State, Action } from "../interfaces";
import filterProjectsByEmployee from "../utils/filterProjectsByEmployee";
import Project from "./Project";
import { EMPLOYEES_KEY } from "../objects";
import Revenue from "./Revenue";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function Employee({ state, dispatch }: Props): JSX.Element {
  const { employeeid } = useParams();
  const employeesProjects = employeeid
    ? filterProjectsByEmployee(employeeid, state.projectsWithNames)
    : [];

  useEffect(() => {
    const getEmployeeName = async () => {
      const employeeRes = await axios.get(
        "https://consulting-projects.academy-faculty.repl.co/api/employees/" +
          employeeid
      );
      dispatch({ type: "setCurrentEmployee", employee: employeeRes.data });
    };
    if (employeeid) {
      getEmployeeName();
      if (!state.recentEmployees.includes(employeeid)) {
        localStorage.setItem(
          EMPLOYEES_KEY,
          JSON.stringify([...state.recentEmployees, employeeid])
        );
        dispatch({
          type: "setRecentEmployees",
          recentEmployees: [...state.recentEmployees, employeeid],
        });
      }
    }
  }, [employeeid, dispatch, state.recentEmployees]);
  return (
    <div>
      <Image
        src={state.currentEmployee.avatar}
        alt="avatar"
        roundedCircle={true}
      />
      <h1>{state.currentEmployee.name}</h1>
      <h2>{state.currentEmployee.role}</h2>
      <Revenue
        allProjects={state.projectsWithNames}
        selectedProjects={employeesProjects}
      />
      <ListGroup variant="flush">
        <h1 className="mx-3 mt-5">Consultancy Projects: </h1>
        {employeesProjects.map((project) => (
          <ListGroup.Item key={project.id}>
            <Project project={project} />
          </ListGroup.Item>
        ))}
      </ListGroup>
    </div>
  );
}
