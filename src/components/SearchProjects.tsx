import { Dispatch, ChangeEvent } from "react";
import {
  Container,
  Dropdown,
  DropdownButton,
  Form,
  FormControl,
  Button,
} from "react-bootstrap";
import { State, Action, Sort } from "../interfaces";
import { emptyFilters } from "../objects";
import FilterCloud from "./FilterCloud";

interface Props {
  state: State;
  dispatch: Dispatch<Action>;
}

export default function SearchProjects({
  state,
  dispatch,
}: Props): JSX.Element {
  return (
    <div>
      <FilterCloud state={state} dispatch={dispatch} />

      <Container fluid className="m-3">
        <div className="d-flex justify-content-center my-2">
          <DropdownButton
            id="dropdown-basic-button"
            title="Clients"
            className="mx-2"
            onSelect={(e: string | null) => {
              if (e)
                dispatch({
                  type: "setFilters",
                  filters: {
                    ...state.filters,
                    clients: [...state.filters.clients, e],
                  },
                });
            }}
          >
            {state.clients.map((client) => (
              <Dropdown.Item eventKey={client.name} key={client.id}>
                {client.name}
              </Dropdown.Item>
            ))}
          </DropdownButton>
          <DropdownButton
            id="dropdown-basic-button"
            title="Employees"
            className="mx-5 mh-80"
            onSelect={(e: string | null) => {
              if (e)
                dispatch({
                  type: "setFilters",
                  filters: {
                    ...state.filters,
                    employees: [...state.filters.employees, e],
                  },
                });
            }}
          >
            {state.employees.map((employee) => (
              <Dropdown.Item eventKey={employee.name} key={employee.id}>
                {employee.name}
              </Dropdown.Item>
            ))}
          </DropdownButton>
          <DropdownButton
            id="dropdown-basic-button"
            title="Sort By"
            className="mx-5"
            onSelect={(e: Sort | null) => {
              if (e) dispatch({ type: "setSort", sort: e });
            }}
          >
            <Dropdown.Item eventKey={"size"}>Size</Dropdown.Item>
            <Dropdown.Item eventKey={"newest-start"}>
              Newest Start
            </Dropdown.Item>
            <Dropdown.Item eventKey={"newest-end"}>Newest End</Dropdown.Item>
            <Dropdown.Item eventKey={"oldest-start"}>
              Oldest Start
            </Dropdown.Item>
            <Dropdown.Item eventKey={"oldest-end"}>Oldest End</Dropdown.Item>
          </DropdownButton>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              onChange={(e: ChangeEvent<HTMLInputElement>) => {
                if (e.target.value)
                  dispatch({
                    type: "setFilters",
                    filters: { ...state.filters, search: e.target.value },
                  });
              }}
            />
          </Form>
          <Button
            variant="outline-danger"
            onClick={() =>
              dispatch({ type: "setFilters", filters: emptyFilters })
            }
          >
            Clear Filters
          </Button>
        </div>

        <div className="d-flex justify-content-center">
          <div className="d-flex border p-1">
            <Form className="d-flex">
              <Form.Control
                type="number"
                placeholder="Min Size"
                className="me-2"
                aria-label="Min Size"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  if (e.target.value)
                    dispatch({
                      type: "setFilters",
                      filters: {
                        ...state.filters,
                        minSize: parseInt(e.target.value),
                      },
                    });
                }}
              />
            </Form>
            <Form className="d-flex">
              <FormControl
                type="number"
                placeholder="Max Size"
                className="me-2"
                aria-label="Max Size"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  if (e.target.value)
                    dispatch({
                      type: "setFilters",
                      filters: {
                        ...state.filters,
                        maxSize: parseInt(e.target.value),
                      },
                    });
                }}
              />
            </Form>
          </div>

          <div className="d-flex border p-1">
            <Form className="d-flex">
              <Form.Label className="align-bottom mx-1">
                Started After
              </Form.Label>
              <Form.Control
                type="date"
                placeholder="From"
                className="me-2"
                aria-label="From"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  if (e.target.value)
                    dispatch({
                      type: "setFilters",
                      filters: {
                        ...state.filters,
                        startedFrom: new Date(e.target.value),
                      },
                    });
                }}
              />
            </Form>
            <Form className="d-flex">
              <Form.Label className="align-bottom mx-1">
                Started Before
              </Form.Label>
              <FormControl
                type="date"
                placeholder="To"
                className="me-2"
                aria-label="To"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  if (e.target.value)
                    dispatch({
                      type: "setFilters",
                      filters: {
                        ...state.filters,
                        startedTo: new Date(e.target.value),
                      },
                    });
                }}
              />
            </Form>
          </div>
          <div className="d-flex border p-1">
            <Form className="d-flex">
              <Form.Label className="align-bottom mx-1">Ended After</Form.Label>
              <Form.Control
                type="date"
                placeholder="From"
                className="me-2"
                aria-label="From"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  if (e.target.value)
                    dispatch({
                      type: "setFilters",
                      filters: {
                        ...state.filters,
                        from: new Date(e.target.value),
                      },
                    });
                }}
              />
            </Form>
            <Form className="d-flex">
              <Form.Label className="align-bottom mx-1">
                Ended Before{" "}
              </Form.Label>
              <FormControl
                type="date"
                placeholder="To"
                className="me-2"
                aria-label="To"
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  if (e.target.value)
                    dispatch({
                      type: "setFilters",
                      filters: {
                        ...state.filters,
                        to: new Date(e.target.value),
                      },
                    });
                }}
              />
            </Form>
          </div>
        </div>
      </Container>
    </div>
  );
}
