import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Header(): JSX.Element {
  return (
    <Navbar bg="light" expand="lg" sticky="top" className="px-5" id="navbar">
      <h1 className="col-sm text-left">&delta;</h1>
      <Navbar.Brand
        className="col-sm text-center"
        onClick={() => window.scrollTo(0, 0)}
      >
        CorpSquad
      </Navbar.Brand>
      <Nav className="flex-row-reverse col-sm">
        <Nav.Link>
          <Link onClick={() => window.scrollTo(0, 0)} to="/recent/employees">
            Recent Employees
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link onClick={() => window.scrollTo(0, 0)} to="/recent/clients">
            Recent Clients
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link onClick={() => window.scrollTo(0, 0)} to="/employees">
            Employees
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link
            onClick={() => window.scrollTo(0, 0)}
            to="/clients"
            id="clients-link"
          >
            Clients
          </Link>
        </Nav.Link>
        <Nav.Link>
          <Link onClick={() => window.scrollTo(0, 0)} to="/">
            Projects
          </Link>
        </Nav.Link>
      </Nav>
    </Navbar>
  );
}
