import { useReducer, useEffect } from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import Client from "./components/Client";
import Clients from "./components/Clients";
import Dashboard from "./components/Dashboard";
import Employee from "./components/Employee";
import Employees from "./components/Employees";
import Header from "./components/Header";
import { Action, State } from "./interfaces";
import { getProjects } from "./utils/getProjects";
import { getEmployees } from "./utils/getEmployees";
import { getClients } from "./utils/getClients";
import { setPropjectsWithNames } from "./utils/setProjectsWithNames";
import setFilteredProjects from "./utils/setFilteredProjects";
import { emptyFilters, emptyEmployee } from "./objects";
import getStoredClients from "./hooks/getStoredClients";
import RecentClients from "./components/RecentClients";
import RecentEmployees from "./components/RecentEmployees";
import getStoredEmployees from "./hooks/getStoredEmployees";

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "setProjects":
      return { ...state, projects: action.projects };
    case "setClients":
      return { ...state, clients: action.clients };
    case "setCurrentClient":
      return { ...state, currentClient: action.client };
    case "setEmployees":
      return { ...state, employees: action.employees };
    case "setCurrentEmployee":
      return { ...state, currentEmployee: action.employee };
    case "setProjectsWithNames":
      return { ...state, projectsWithNames: action.projectsWithNames };
    case "setFilters":
      return { ...state, filters: action.filters };
    case "setSort":
      return { ...state, sort: action.sort };
    case "setFilteredProjects":
      return { ...state, filteredProjects: action.filteredProjects };
    case "setRecentClients":
      return { ...state, recentClients: action.recentClients };
    case "setRecentEmployees":
      return { ...state, recentEmployees: action.recentEmployees };
  }
};

function App(): JSX.Element {
  const [state, dispatch] = useReducer(reducer, {
    projects: [],
    clients: [],
    currentClient: { id: "", name: "" },
    employees: [],
    currentEmployee: emptyEmployee,
    projectsWithNames: [],
    filteredProjects: [],
    filters: emptyFilters,
    sort: "",
    recentClients: [],
    recentEmployees: [],
  });
  //All useEffects happen here
  useEffect(() => {
    if (state.projects.length < 1) getProjects(dispatch); //only call once
    if (state.employees.length < 1) getEmployees(dispatch); //only call once
    if (state.clients.length < 1) getClients(dispatch); //only call once
    if (state.projectsWithNames.length < 1)
      setPropjectsWithNames(
        state.projects,
        state.clients,
        state.employees,
        dispatch
      );
    //only run if we have the names array
  }, [
    state.projects,
    state.clients,
    state.employees,
    state.projectsWithNames,
    dispatch,
    state.recentClients.length,
  ]);

  useEffect(() => {
    if (state.projectsWithNames.length > 0)
      setFilteredProjects(
        state.projectsWithNames,
        state.filters,
        state.sort,
        dispatch
      );
  }, [state.projectsWithNames, state.filters, state.sort, dispatch]);

  useEffect(() => {
    getStoredClients(dispatch);
    getStoredEmployees(dispatch);
  }, []);
  return (
    <BrowserRouter>
      <Header />
      <div style={{ margin: "10px" }}>
        <Routes>
          <Route
            path="/"
            element={<Dashboard state={state} dispatch={dispatch} />}
          />
          <Route
            path="/clients"
            element={<Clients state={state} dispatch={dispatch} />}
          />
          <Route
            path="/clients/:clientid"
            element={<Client state={state} dispatch={dispatch} />}
          />
          <Route
            path="/employees"
            element={<Employees state={state} dispatch={dispatch} />}
          />
          <Route
            path="/employees/:employeeid"
            element={<Employee state={state} dispatch={dispatch} />}
          />
          <Route
            path="/recent/clients"
            element={
              <RecentClients
                recentClients={state.recentClients}
                allClients={state.clients}
              />
            }
          />
          <Route
            path="/recent/employees"
            element={
              <RecentEmployees
                recentEmployees={state.recentEmployees}
                allEmployees={state.employees}
              />
            }
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
