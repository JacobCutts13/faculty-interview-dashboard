import filterProjectsByClient from "./filterProjectsbyClient";
import { exampleProjectsWithNames } from "../objects";

test("filterProjectsByClient takes an array of projects and sums contract.size", () => {
  expect(
    filterProjectsByClient("2ebfad09bacb0c0cc031b5fa", [
      exampleProjectsWithNames[0],
    ])
  ).toStrictEqual([exampleProjectsWithNames[0]]);
  expect(
    filterProjectsByClient("2ebfad09bacb0c0cc", exampleProjectsWithNames)
  ).toStrictEqual([]);
  expect(filterProjectsByClient("abfe2a2ede38bdd9fbee70f4", [])).toStrictEqual(
    []
  );
});
