import { Dispatch } from "react";
import axios from "axios";
import { Action } from "../interfaces";

export const getEmployees = async (
  dispatch: Dispatch<Action>
): Promise<void> => {
  try {
    const employeesRes = await axios.get(
      "https://consulting-projects.academy-faculty.repl.co/api/employees"
    );
    dispatch({ type: "setEmployees", employees: employeesRes.data });
  } catch (error) {
    console.error(error);
  }
};
