import { ProjectWithNames } from "../interfaces";

export default function filterProjectsByEmployee(
  employeeId: string,
  allProjects: ProjectWithNames[]
): ProjectWithNames[] {
  const filteredProjects = allProjects.filter((project) =>
    project.employeeIds.includes(employeeId)
  );
  return filteredProjects;
}
