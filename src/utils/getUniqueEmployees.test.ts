import getUniqueEmployees from "./getUniqueEmployees";
import { exampleProjectsWithNames } from "../objects";

test("getUniqueEmployees gets the names and ids of employees a client has worked with", () => {
  expect(getUniqueEmployees([exampleProjectsWithNames[4]])).toStrictEqual([
    { id: "b7bfd67f426aedfe8aab7ed1", name: "Ms. Ricky Schoen" },
  ]);
  expect(getUniqueEmployees([])).toStrictEqual([]);
});
