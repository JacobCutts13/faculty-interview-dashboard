import { ProjectWithNames, Sort } from "../interfaces";

export default function sortProjects(
  projects: ProjectWithNames[],
  sort: Sort
): ProjectWithNames[] {
  if (sort === "size") {
    return projects.sort(
      (a, b) => parseFloat(b.contract.size) - parseFloat(a.contract.size)
    );
  }
  if (sort === "newest-end") {
    return projects.sort(
      (a, b) => Date.parse(b.contract.endDate) - Date.parse(a.contract.endDate)
    );
  }
  if (sort === "oldest-end") {
    return projects.sort(
      (a, b) => Date.parse(a.contract.endDate) - Date.parse(b.contract.endDate)
    );
  }
  if (sort === "newest-start") {
    return projects.sort(
      (a, b) =>
        Date.parse(b.contract.startDate) - Date.parse(a.contract.startDate)
    );
  }
  if (sort === "oldest-start") {
    return projects.sort(
      (a, b) =>
        Date.parse(a.contract.startDate) - Date.parse(b.contract.startDate)
    );
  }
  return projects;
}
