import { Employee } from "../interfaces";

export const getEmployeeNames = (
  employees: Employee[],
  employeeIds: string[]
): string[] => {
  const Projectemployees = employees.filter((employee) =>
    employeeIds.includes(employee.id)
  );
  const employeeNames = Projectemployees.map((employee) => employee.name);
  return employeeNames;
};
