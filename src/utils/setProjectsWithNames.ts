import { Dispatch } from "react";
import { Action, Client, Employee, Project } from "../interfaces";
import { getClientName } from "./getClientName";
import { getEmployeeNames } from "./getEmployeesName";

export const setPropjectsWithNames = (
  projects: Project[],
  clients: Client[],
  employees: Employee[],
  dispatch: Dispatch<Action>
): void => {
  if (projects.length < 1 || clients.length < 1 || employees.length < 1) return;
  const projectsWithNames = projects.map((project) => {
    const clientName = getClientName(clients, project.clientId);
    const employeeNames = getEmployeeNames(employees, project.employeeIds);
    return { ...project, clientName: clientName, employeeNames: employeeNames };
  });
  dispatch({
    type: "setProjectsWithNames",
    projectsWithNames: projectsWithNames,
  });
};
