import { Dispatch } from "react";
import { Action, Filters, ProjectWithNames, Sort } from "../interfaces";
import sortProjects from "./sortProjects";

export default function setFilteredProjects(
  allProjects: ProjectWithNames[],
  filters: Filters,
  sort: Sort,
  dispatch: Dispatch<Action>
): void {
  const sortedProjects = sortProjects(allProjects, sort);
  const filteredProjects = sortedProjects.filter((project) => {
    //clients
    if (filters.clients.length !== 0) {
      if (!filters.clients.includes(project.clientName)) {
        return false;
      }
    }
    //employees
    if (filters.employees.length !== 0) {
      if (
        !project.employeeNames.some((name) => filters.employees.includes(name))
      ) {
        return false;
      }
    }
    //end from
    if (filters.from !== "any") {
      if (Date.parse(project.contract.endDate) < filters.from.getTime()) {
        return false;
      }
    }
    //end to
    if (filters.to !== "any") {
      if (Date.parse(project.contract.endDate) > filters.to.getTime()) {
        return false;
      }
    }
    //start from
    if (filters.startedFrom !== "any") {
      if (
        Date.parse(project.contract.startDate) < filters.startedFrom.getTime()
      ) {
        return false;
      }
    }
    //started to
    if (filters.startedTo !== "any") {
      if (
        Date.parse(project.contract.startDate) > filters.startedTo.getTime()
      ) {
        return false;
      }
    }
    //typed search
    if (
      !(
        project.clientName
          .toLowerCase()
          .includes(filters.search.toLowerCase()) ||
        project.employeeNames.some((name) =>
          name.toLowerCase().includes(filters.search.toLowerCase())
        )
      )
    ) {
      return false;
    }
    //min size
    if (filters.minSize !== -1) {
      if (parseFloat(project.contract.size) < filters.minSize) {
        return false;
      }
    }
    //max size
    if (filters.maxSize !== -1) {
      if (parseFloat(project.contract.size) > filters.maxSize) {
        return false;
      }
    }
    return true;
  });
  dispatch({
    type: "setFilteredProjects",
    filteredProjects: filteredProjects,
  });
}
