import { ProjectWithNames } from "../interfaces";

export default function calcRevenue(projects: ProjectWithNames[]): number {
  const revenue: number = projects.reduce(
    (prev, curr) => prev + parseFloat(curr.contract.size),
    0
  );
  return revenue;
}
