import { Client } from "../interfaces";

export const getClientName = (clients: Client[], clientId: string): string => {
  const client = clients.filter((client) => client.id === clientId);
  if (client[0]) return client[0].name;
  return "";
};
