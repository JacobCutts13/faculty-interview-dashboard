import { Dispatch } from "react";
import axios from "axios";
import { Action } from "../interfaces";

export const getClients = async (dispatch: Dispatch<Action>): Promise<void> => {
  try {
    const clientsRes = await axios.get(
      "https://consulting-projects.academy-faculty.repl.co/api/clients"
    );
    dispatch({ type: "setClients", clients: clientsRes.data });
  } catch (error) {
    console.error(error);
  }
};
