import { ProjectWithNames } from "../interfaces";

export default function filterProjectsByClient(
  clientId: string,
  allProjects: ProjectWithNames[]
): ProjectWithNames[] {
  const filteredProjects = allProjects.filter(
    (project) => project.clientId === clientId
  );
  return filteredProjects;
}
