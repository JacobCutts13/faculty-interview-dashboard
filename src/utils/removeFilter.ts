import { Dispatch } from "react";
import { Action, State } from "../interfaces";

export default function removeFilter(
  state: State,
  dispatch: Dispatch<Action>,
  filterType: string,
  filterValue: string
): void {
  if (filterType === "client") {
    const newClients = state.filters.clients.filter(
      (client) => client !== filterValue
    );
    dispatch({
      type: "setFilters",
      filters: { ...state.filters, clients: newClients },
    });
    return;
  }
  if (filterType === "employee") {
    const newEmployees = state.filters.employees.filter(
      (employee) => employee !== filterValue
    );
    dispatch({
      type: "setFilters",
      filters: { ...state.filters, employees: newEmployees },
    });
    return;
  }
  return;
}
