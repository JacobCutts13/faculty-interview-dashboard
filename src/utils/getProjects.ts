import { Dispatch } from "react";
import axios from "axios";
import { Action } from "../interfaces";

export const getProjects = async (
  dispatch: Dispatch<Action>
): Promise<void> => {
  try {
    const projectsRes = await axios.get(
      "https://consulting-projects.academy-faculty.repl.co/api/projects"
    );
    dispatch({ type: "setProjects", projects: projectsRes.data });
  } catch (error) {
    console.error(error);
  }
};
