import calcRevenue from "./calcRevenue";
import { exampleProjectsWithNames } from "../objects";

test("calcRevenue takes an array of projects and sums contract.size", () => {
  expect(calcRevenue([exampleProjectsWithNames[0]])).toBe(47362.95);
  expect(calcRevenue(exampleProjectsWithNames)).toBe(1743621.6700000004);
  expect(calcRevenue([])).toBe(0);
});
