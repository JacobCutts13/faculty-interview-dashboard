import { Client, ProjectWithNames } from "../interfaces";

export default function getUniqueEmployees(
  projects: ProjectWithNames[]
): Client[] {
  const employeeNames: Client[] = [];
  projects.forEach((project) =>
    project.employeeIds.forEach((id, index) => {
      if (!employeeNames.some((employee) => employee.id === id)) {
        employeeNames.push({ id: id, name: project.employeeNames[index] });
      }
    })
  );
  return employeeNames;
}
