import { Dispatch } from "react";
import { Action } from "../interfaces";
import { EMPLOYEES_KEY } from "../objects";

//call dispatch and pass it local values
export default function getStoredEmployees(dispatch: Dispatch<Action>): void {
  const jsonValue = localStorage.getItem(EMPLOYEES_KEY);
  if (jsonValue !== null) {
    dispatch({
      type: "setRecentEmployees",
      recentEmployees: JSON.parse(jsonValue),
    });
  }
  return;
}
