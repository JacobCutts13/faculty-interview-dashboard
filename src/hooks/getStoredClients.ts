import { Dispatch } from "react";
import { Action } from "../interfaces";
import { CLIENTS_KEY } from "../objects";

//call dispatch and pass it local values
export default function getStoredClients(dispatch: Dispatch<Action>): void {
  const jsonValue = localStorage.getItem(CLIENTS_KEY);
  if (jsonValue !== null) {
    dispatch({
      type: "setRecentClients",
      recentClients: JSON.parse(jsonValue),
    });
  }
  return;
}
