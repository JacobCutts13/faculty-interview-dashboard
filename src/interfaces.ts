export interface Project {
  id: string;
  clientId: string;
  employeeIds: string[];
  contract: {
    startDate: string;
    endDate: string;
    size: string;
  };
}

export interface ProjectWithNames extends Project {
  clientName: string;
  employeeNames: string[];
}

export interface State {
  projects: Project[];
  clients: Client[];
  currentClient: Client;
  employees: Employee[];
  currentEmployee: Employee;
  projectsWithNames: ProjectWithNames[];
  filteredProjects: ProjectWithNames[];
  filters: Filters;
  sort: Sort;
  recentClients: string[];
  recentEmployees: string[];
}

export type Action =
  | { type: "setProjects"; projects: Project[] }
  | { type: "setClients"; clients: Client[] }
  | { type: "setCurrentClient"; client: Client }
  | { type: "setEmployees"; employees: Employee[] }
  | { type: "setCurrentEmployee"; employee: Employee }
  | { type: "setProjectsWithNames"; projectsWithNames: ProjectWithNames[] }
  | { type: "setFilters"; filters: Filters }
  | { type: "setSort"; sort: Sort }
  | { type: "setFilteredProjects"; filteredProjects: ProjectWithNames[] }
  | { type: "setRecentClients"; recentClients: string[] }
  | { type: "setRecentEmployees"; recentEmployees: string[] };

export interface Employee {
  id: string;
  name: string;
  role: string;
  avatar: string;
}

export interface Client {
  id: string;
  name: string;
}

export interface ProjectProps {
  project: Project;
}

export interface Filters {
  clients: string[];
  employees: string[];
  from: Date | "any";
  to: Date | "any";
  startedFrom: Date | "any";
  startedTo: Date | "any";
  search: string;
  minSize: number;
  maxSize: number;
}

export type Sort = string; //"size" | "newest" | "oldest";
